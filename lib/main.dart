import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/providers/transaction.dart';
import 'package:dingtoimc/providers/user.dart';
import 'package:dingtoimc/screens/phone_input_screen.dart';
import 'package:dingtoimc/screens/telephone_check_screen.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dingtoimc/screens/diamond_rating_screen.dart';
import 'package:dingtoimc/screens/diamond_rating_buyer_screen.dart';
import 'package:dingtoimc/screens/imei_input_screen.dart';
import 'package:dingtoimc/screens/login_screen.dart';
import 'package:dingtoimc/screens/physical_grading_screen.dart';
import 'package:dingtoimc/screens/scanner_basic_screen.dart';
import 'package:dingtoimc/screens/thank_screen.dart';
import 'package:dingtoimc/screens/touch_screen.dart';
import 'package:dingtoimc/screens/transaction_code_screen.dart';
import 'package:dingtoimc/screens/welcome_start_screen.dart';
import 'package:dingtoimc/screens/scanner_confirm_screen.dart';
import 'package:dingtoimc/screens/blacklist_screen.dart';
import 'package:dingtoimc/screens/iCloud_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
    );

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => User(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Device(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Transaction(),
        ),
      ],
      child: Consumer<User>(
        builder: (ctx, auth, _) => MaterialApp(
          title: 'Dingtoi MC',
          debugShowCheckedModeBanner: true,
          theme: ThemeData(
            primarySwatch: Colors.indigo,
            accentColor: Colors.indigo,
            textTheme: TextTheme(
              bodyText1: TextStyle(
                fontSize: 16.0,
                fontFamily: 'Proxima',
                color: const Color(0xFF000000),
              ),
              bodyText2: TextStyle(
                fontSize: 16.0,
                fontFamily: 'Proxima',
                color: const Color(0xFF000000),
              ),
              button: TextStyle(
                fontSize: 16.0,
                fontFamily: 'Proxima',
                color: const Color(0xFFFFFFFF),
                letterSpacing: 0.627451,
              ),
            ),
            fontFamily: 'Proxima',
            //scaffoldBackgroundColor: const Color(0xFFF2F2F2),
            scaffoldBackgroundColor: const Color(0xFFFFFFFF),
            appBarTheme: AppBarTheme(
              color: const Color(0xFFFFFFFF),
              elevation: 0.0,
            ),
          ),
          home: LoginScreen(),
          routes: {
            WelcomeStartScreen.routeName: (ctx) => WelcomeStartScreen(),
            LoginScreen.routeName: (ctx) => LoginScreen(),
            ScannerBasicScreen.routeName: (ctx) => ScannerBasicScreen(),
            DiamondRatingScreen.routeName: (ctx) => DiamondRatingScreen(),
            DiamondRatingBuyerScreen.routeName: (ctx) =>
                DiamondRatingBuyerScreen(),
            TouchScreen.routeName: (ctx) => TouchScreen(),
            ThankScreen.routeName: (ctx) => ThankScreen(),
            PhysicalGradingScreen.routeName: (ctx) => PhysicalGradingScreen(),
            TransactionCodeScreen.routeName: (ctx) => TransactionCodeScreen(),
            ImeiInputScreen.routeName: (ctx) => ImeiInputScreen(),
            ScannerConfirmScreen.routeName: (ctx) => ScannerConfirmScreen(),
            BlacklistScreen.routeName: (ctx) => BlacklistScreen(),
            ICloudScreen.routeName: (ctx) => ICloudScreen(),
            PhoneInputScreen.routeName: (ctx) => PhoneInputScreen(),
            TelephoneCheckScreen.routeName: (ctx) => TelephoneCheckScreen(),
          },
        ),
      ),
    );
  }
}
