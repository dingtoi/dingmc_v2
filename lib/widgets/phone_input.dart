import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/screens/telephone_check_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/button_gradient_loading.dart';
import 'package:dingtoimc/widgets/input.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PhoneInputForm extends StatefulWidget {
  final String _userType;

  PhoneInputForm(this._userType);

  @override
  _PhoneInputFormState createState() => _PhoneInputFormState();
}

class _PhoneInputFormState extends State<PhoneInputForm> {
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;

  String _phone = '';
  String _token = '';

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Functions.onGetToken().then((token) {
      setState(() {
        _token = token;
      });
    });
  }

  void _saveForm(device) {
    _form.currentState.save();
    if (_phone == '') {
      Functions.onConfirmError(context, 'Phone Error', 'Phone must not empty');
    } else {
      Navigator.of(context)
          .pushReplacementNamed(TelephoneCheckScreen.routeName);
    }
  }

  @override
  Widget build(BuildContext context) {
    final device = Provider.of<Device>(context, listen: false);
    final phoneField = Input(
      'Phone Number',
      'phone',
      null,
      (_) {},
      (_) {},
      (value) {
        _phone = value;
      },
    );

    return Form(
      key: _form,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextCustom('Phone Number'),
          SizedBox(height: 10),
          phoneField,
          SpaceCustom(),
          SpaceCustom(),
          Center(
            child: _isLoading
                ? ButtonGradientLoading(173.0)
                : ButtonGradient('Accept', 173.0, () {
                    _saveForm(device);
                  }),
          ),
        ],
      ),
    );
  }
}
