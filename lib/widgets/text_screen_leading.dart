import 'package:flutter/material.dart';

class TextScreenLeading extends StatelessWidget {
  final String message;

  TextScreenLeading(this.message);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 3.0),
      child: Opacity(
        opacity: 0.7,
        child: Text(
          message.toUpperCase(),
          style: TextStyle(
            fontSize: 24,
            letterSpacing: 5,
          ),
        ),
      ),
    );
  }
}
