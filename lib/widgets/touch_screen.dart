import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/scanner_basic_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TouchScreenCustom extends StatefulWidget {
  @override
  _TouchScreenCustomState createState() => _TouchScreenCustomState();
}

class _TouchScreenCustomState extends State<TouchScreenCustom> {
  List<Widget> _rectangles = [];
  List _touchChecked = [];
  int _numChecked = 0;

  @override
  void initState() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    List touchChecked = [];

    for (int i = 0;
        i < ConfigCustom().touchscreenRow * ConfigCustom().touchscreenCol;
        i++) {
      touchChecked.add({
        'checked': false,
      });
    }

    setState(() {
      _touchChecked = touchChecked;
    });
    refreshRectangles();
    super.didChangeDependencies();
  }

  calculateToDraw(Offset position) {
    double itemWidth =
        MediaQuery.of(context).size.width / ConfigCustom().touchscreenRow;
    double itemHeight =
        MediaQuery.of(context).size.height / ConfigCustom().touchscreenCol;

    int _row = (position.dx / itemWidth).ceil();
    int _col = (position.dy / itemHeight).ceil();
    int _index = (_row + ((_col - 1) * ConfigCustom().touchscreenRow)) - 1;
    if (!_touchChecked[_index]['checked']) {
      _numChecked++;
      setState(() {
        _touchChecked[_index]['checked'] = true;
      });
      refreshRectangles();
    }
  }

  forceToDraw(index) {
    if (!_touchChecked[index]['checked']) {
      _numChecked++;
      setState(() {
        _touchChecked[index]['checked'] = true;
      });
      refreshRectangles();
    }
  }

  refreshRectangles() {
    List<Widget> rectangles = [];
    for (var i = 0;
        i < ConfigCustom().touchscreenRow * ConfigCustom().touchscreenCol;
        i++) {
      rectangles.add(GestureDetector(
        onTap: () {
          forceToDraw(i);
        },
        onTapDown: (_) {
          forceToDraw(i);
        },
        onHorizontalDragDown: (_) {
          forceToDraw(i);
        },
        onHorizontalDragStart: (DragStartDetails details) {
          calculateToDraw(details.globalPosition);
        },
        onHorizontalDragUpdate: (DragUpdateDetails details) {
          calculateToDraw(details.globalPosition);
        },
        onHorizontalDragEnd: (DragEndDetails details) {
          forceToDraw(i);
        },
        onVerticalDragDown: (_) {
          forceToDraw(i);
        },
        onVerticalDragStart: (DragStartDetails details) {
          calculateToDraw(details.globalPosition);
        },
        onVerticalDragUpdate: (DragUpdateDetails details) {
          calculateToDraw(details.globalPosition);
        },
        onVerticalDragEnd: (DragEndDetails details) {
          forceToDraw(i);
        },
        child: Container(
          decoration: BoxDecoration(
              color: _touchChecked[i]['checked']
                  ? Color(0xFF303A96)
                  : Colors.white,
              border: Border.fromBorderSide(BorderSide.none)),
        ),
      ));
    }

    setState(() {
      _rectangles = rectangles;
    });

    if (_numChecked ==
        ConfigCustom().touchscreenRow * ConfigCustom().touchscreenCol) {
      Functions.onConfirmSuccessTouchscreen(
          context, 'Touchscreen', 'Thank you. Touchscreen is working fine.',
          () {
        Navigator.of(context)
            .pushReplacementNamed(ScannerBasicScreen.routeName);
      });
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    double itemWidth =
        MediaQuery.of(context).size.width / ConfigCustom().touchscreenRow;
    double itemHeight =
        MediaQuery.of(context).size.height / ConfigCustom().touchscreenCol;

    return Container(
      color: Color(0xFF303A96),
      child: GridView.extent(
          maxCrossAxisExtent: itemWidth,
          childAspectRatio: itemWidth / itemHeight,
          padding: const EdgeInsets.all(0),
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
          children: _rectangles),
    );
  }
}
