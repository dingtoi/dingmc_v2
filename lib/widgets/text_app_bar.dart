import 'package:flutter/material.dart';

class TextAppBar extends StatelessWidget {
  final String message;

  TextAppBar(this.message);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Opacity(
        opacity: 0.7,
        child: Text(
          message,
          style: TextStyle(
            color: const Color(0xFF000000),
            fontWeight: FontWeight.bold,
            fontSize: 18,
            letterSpacing: 0.705882,
          ),
        ),
      ),
    );
  }
}
