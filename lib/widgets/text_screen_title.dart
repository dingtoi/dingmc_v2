import 'package:flutter/material.dart';

class TextScreenTitle extends StatelessWidget {
  final String message;

  TextScreenTitle(this.message);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Opacity(
        opacity: 0.7,
        child: Text(
          message,
          style: TextStyle(
            fontSize: 32,
            letterSpacing: 2,
            color: const Color(0xFF000000),
          ),
        ),
      ),
    );
  }
}
