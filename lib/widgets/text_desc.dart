import 'package:dingtoimc/helpers/functions.dart';
import 'package:flutter/material.dart';

class TextDesc extends StatelessWidget {
  final String message;

  TextDesc(this.message);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Opacity(
        opacity: 0.7,
        child: Functions.isEmpty(message)
            ? Text(
                'Not verified',
                style: TextStyle(
                  fontSize: 11,
                  color: const Color(0xFF000000),
                ),
              )
            : Text(
                message,
                style: TextStyle(
                  fontSize: 11,
                  color: const Color(0xFF000000),
                ),
              ),
      ),
    );
  }
}
