import 'package:flutter/material.dart';

class TextCustom extends StatelessWidget {
  final String message;

  TextCustom(this.message);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Opacity(
        opacity: 0.7,
        child: Text(
          message,
          style: TextStyle(
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}
