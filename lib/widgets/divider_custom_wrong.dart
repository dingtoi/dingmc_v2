import 'package:flutter/material.dart';

class DividerCustomWrong extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Divider(
        color: new Color(0xFFE6CFCF),
        height: 25,
        thickness: 1,
      ),
    );
  }
}
