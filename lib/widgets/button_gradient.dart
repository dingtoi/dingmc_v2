import 'package:flutter/material.dart';

class ButtonGradient extends StatelessWidget {
  final String message;
  final double width;
  final Function onPressed;

  ButtonGradient(this.message, this.width, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 40.0,
      child: RaisedButton(
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(35.5),
        ),
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Color(0xFF303A96),
                Color(0xFF242E88),
              ],
            ),
            borderRadius: BorderRadius.circular(35.5),
          ),
          child: Container(
            constraints: BoxConstraints(maxWidth: width, minHeight: 40.0),
            alignment: Alignment.center,
            child: Text(
              message.toUpperCase(),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
