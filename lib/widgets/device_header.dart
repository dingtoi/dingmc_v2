import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/widgets/divider_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:flutter/material.dart';

class DeviceHeader extends StatelessWidget {
  final String _userType;
  final String _model;
  final String _transactionCode;

  DeviceHeader(this._userType, this._model, this._transactionCode);

  Widget userTypeDisplay() {
    if (_userType == ConfigCustom().anonymous ||
        _userType == ConfigCustom().none) {
      return Center();
    } else {
      if (Functions.isEmpty(_transactionCode))
        return Center();
      else
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DividerCustom(),
            TextDesc('Transaction Code'),
            SizedBox(height: 4),
            TextCustom(_transactionCode),
          ],
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFF6F6F6),
      height: 125,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 125,
                  child: SizedBox(
                    width: 93,
                    child: Image.asset(
                      'assets/app/phone.png',
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextDesc('Device Name'),
                        SizedBox(height: 4),
                        TextCustom(_model),
                        userTypeDisplay(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
