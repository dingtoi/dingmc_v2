import 'package:flutter/material.dart';

class ButtonSmall extends StatelessWidget {
  final String message;
  final double width;
  final Function onPressed;

  ButtonSmall(this.message, this.width, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: 28.0,
      child: RaisedButton(
        onPressed: onPressed,
        textColor: Colors.white,
        padding: const EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                Color(0xFF303A96),
                Color(0xFF242E88),
              ],
            ),
          ),
          child: Container(
            constraints: BoxConstraints(maxWidth: width, minHeight: 28.0),
            alignment: Alignment.center,
            child: Text(
              message.toUpperCase(),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
