import 'package:flutter/material.dart';

class DividerCustom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Divider(
        color: new Color(0xFFEDECEC),
        height: 25,
        thickness: 1,
      ),
    );
  }
}
