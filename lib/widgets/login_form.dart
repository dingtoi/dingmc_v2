import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/user.dart';
import 'package:dingtoimc/screens/transaction_code_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/button_gradient_loading.dart';
import 'package:dingtoimc/widgets/input.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _passwordFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;

  String _email = '';
  String _password = '';

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    _passwordFocusNode.dispose();
  }

  void _saveForm() {
    _form.currentState.save();
    final user = Provider.of<User>(context, listen: false);
    if (_email == '') {
      Functions.onConfirmError(context, 'Login Error', 'Email must not empty');
    } else if (_password == '') {
      Functions.onConfirmError(
          context, 'Login Error', 'Password must not empty');
    } else if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_email)) {
      Functions.onConfirmError(context, 'Login Error', 'Email Wrong !');
    } else {
      setState(() {
        _isLoading = true;
      });
      user.login(_email, _password).then((res) {
        _form.currentState.reset();
        Map body = json.decode(res.body);
        setState(() {
          _isLoading = false;
        });
        if (!body.containsKey('accessToken')) {
          Functions.onConfirmError(
              context, 'Login Error', 'Email or Password Wrong');
        } else {
          user.setUser(body).then((_) {
            Navigator.of(context)
                .pushReplacementNamed(TransactionCodeScreen.routeName);
            final snackBar = SnackBar(content: Text('Login Successfully!'));
            Scaffold.of(context).showSnackBar(snackBar);
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final emailField = Input(
      'Your Email Address',
      'email',
      null,
      (_) {},
      (_) {},
      (value) {
        _email = value;
      },
    );
    final passwordField = Input(
      'Your Password',
      'password',
      _passwordFocusNode,
      (_) {},
      (_) {},
      (value) {
        _password = value;
      },
    );

    return Form(
      key: _form,
      child: Column(
        children: <Widget>[
          emailField,
          SpaceCustom(),
          passwordField,
          SpaceCustom(),
          SpaceCustom(),
          _isLoading
              ? ButtonGradientLoading(173.0)
              : ButtonGradient('Login', 173.0, () {
                  _saveForm();
                }),
        ],
      ),
    );
  }
}
