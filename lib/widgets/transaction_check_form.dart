import 'dart:convert';

import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/providers/transaction.dart';
import 'package:dingtoimc/screens/thank_screen.dart';
import 'package:dingtoimc/screens/welcome_start_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/button_gradient_loading.dart';
import 'package:dingtoimc/widgets/input.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TransactionCheckForm extends StatefulWidget {
  @override
  _TransactionCheckFormState createState() => _TransactionCheckFormState();
}

class _TransactionCheckFormState extends State<TransactionCheckForm> {
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;

  String _transaction = '';

  @override
  void dispose() {
    super.dispose();
  }

  void _saveForm() {
    _form.currentState.save();
    final transaction = Provider.of<Transaction>(context, listen: false);
    final device = Provider.of<Device>(context, listen: false);

    if (_transaction == '') {
      Functions.onConfirmError(
          context, 'Transaction Error', 'Transaction must not empty');
    } else {
      setState(() {
        _isLoading = true;
      });

      Functions.onGetUserInfo().then((userInfo) {
        transaction
            .checkTransactionCode(
                _transaction, userInfo['userId'], userInfo['token'])
            .then((res) {
          _form.currentState.reset();
          setState(() {
            _isLoading = false;
          });
          Map body = json.decode(res.body);
          if (body.containsKey('error')) {
            Functions.onConfirmError(
                context, 'Transaction Error', 'Transaction Wrong !');
          } else {
            Functions.onConfirmImei(context, body['response']['imei'], () {
              transaction.setTransactionCode(_transaction).then((_) {
                device.setImei(body['response']['imei']).then((_) {
                  transaction
                      .setTransactionType(body['response']['type'])
                      .then((_) {
                    Navigator.of(context)
                        .pushReplacementNamed(WelcomeStartScreen.routeName);
                  });
                });
              });
            }, () {
              Navigator.of(context).pushReplacementNamed(ThankScreen.routeName);
            });
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final transactionField = Input(
      'Transaction Code',
      'text',
      null,
      (_) {},
      (_) {},
      (value) {
        _transaction = value;
      },
    );

    return Form(
      key: _form,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextCustom('Transaction Code'),
          SizedBox(height: 10),
          transactionField,
          SpaceCustom(),
          SpaceCustom(),
          Center(
            child: _isLoading
                ? ButtonGradientLoading(173.0)
                : ButtonGradient('Check', 173.0, () {
                    _saveForm();
                  }),
          ),
        ],
      ),
    );
  }
}
