import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/screens/blacklist_screen.dart';
import 'package:dingtoimc/screens/physical_grading_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/button_gradient_loading.dart';
import 'package:dingtoimc/widgets/input.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class ImeiCheckForm extends StatefulWidget {
  final String _userType;

  ImeiCheckForm(this._userType);

  @override
  _ImeiCheckFormState createState() => _ImeiCheckFormState();
}

class _ImeiCheckFormState extends State<ImeiCheckForm> {
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;

  String _imei = '';
  String _token = '';

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    Functions.onGetToken().then((token) {
      setState(() {
        _token = token;
      });
    });
  }

  Future<void> _saveForm(device) async {
    _form.currentState.save();
    if (_imei == '') {
      Functions.onConfirmError(context, 'Imei Error', 'Imei must not empty');
    } else {
      setState(() {
        _isLoading = true;
      });
      var res = await device.checkImei(_imei, _token);
      _form.currentState.reset();
      Map body = json.decode(res.body);
      if (!body['isSuccess']) {
        Functions.onConfirmError(context, 'Imei Error', 'Imei not match');
      } else {
        await device.setImei(_imei);
        if (widget._userType == ConfigCustom().fee) {
          final device = Provider.of<Device>(context, listen: false);

          Functions.onConfirmShareLocation(context, 'Share Location',
              'Do you want to allow to share Location to check blacklist', () {
            Functions.getLocation().then((location) {
              device
                  .setLocation(location.longitude, location.latitude)
                  .then((_) {
                Navigator.of(context)
                    .pushReplacementNamed(BlacklistScreen.routeName);
              });
            });
          }, () {
            Navigator.of(context)
                .pushReplacementNamed(BlacklistScreen.routeName);
          });
        } else if (widget._userType == ConfigCustom().free) {
          Navigator.of(context)
              .pushReplacementNamed(PhysicalGradingScreen.routeName);
        }
        final snackBar = SnackBar(content: Text('Check Imei Successfully!'));
        Scaffold.of(context).showSnackBar(snackBar);
      }

      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final device = Provider.of<Device>(context, listen: false);
    final transactionField = Input(
      'IMEI Device',
      'text',
      null,
      (_) {},
      (_) {},
      (value) {
        _imei = value;
      },
    );

    return Form(
      key: _form,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextCustom('IMEI Device'),
          SizedBox(height: 10),
          transactionField,
          SpaceCustom(),
          SpaceCustom(),
          Center(
            child: _isLoading
                ? ButtonGradientLoading(173.0)
                : ButtonGradient('Check', 173.0, () {
                    _saveForm(device);
                  }),
          ),
        ],
      ),
    );
  }
}
