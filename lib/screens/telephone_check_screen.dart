import 'dart:io' show Platform;
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/physical_grading_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/device_header.dart';
import 'package:dingtoimc/widgets/divider_custom.dart';
import 'package:dingtoimc/widgets/divider_custom_wrong.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_app_bar.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:flutter/material.dart';

class TelephoneCheckScreen extends StatefulWidget {
  static const routeName = '/telephone_check';

  @override
  _TelephoneCheckScreenState createState() => _TelephoneCheckScreenState();
}

class _TelephoneCheckScreenState extends State<TelephoneCheckScreen> {
  String _userType = '';
  bool _isLoading = false;
  String _transactionCode = '';
  String _model = '';

  Future _initPlatform() async {
    String model = '';

    if (!mounted) return;

    setState(() {
      _isLoading = true;
    });

    Map user = await Functions.onInitDeviceScanner(context);
    setState(() {
      _userType = user['userType'];
      _transactionCode = user['transactionCode'];
    });
    if (Platform.isAndroid) {}

    setState(() {
      _isLoading = false;
      _model = model;
    });
  }

  @override
  void initState() {
    super.initState();
    _initPlatform();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final appBar = AppBar(
      title: TextAppBar('Advanced Scanner Report'),
    );

    return Scaffold(
      appBar: appBar,
      body: WillPopScope(
        onWillPop: () {
          return Functions.onBackPressedLogin(context);
        },
        child: _isLoading
            ? Loading()
            : Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 127.0, 0, 57),
                    child: ListView(
                      children: <Widget>[
                        SpaceCustom(),
                        Container(
                          color: const Color(0xFFF6F6F6),
                          child: Padding(
                            padding:
                                const EdgeInsets.fromLTRB(20.0, 15, 20.0, 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                TextCustom('ADVANCED'),
                              ],
                            ),
                          ),
                        ),
                        SpaceCustom(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(9.0),
                              color: const Color(0xFFFBE7E7),
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0xFF90A4AE),
                                  blurRadius: 3.0,
                                  offset: const Offset(0.0, 2.0),
                                )
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        height: 45,
                                        child: SizedBox(
                                          child: Image.asset(
                                            'assets/app/icon_call.png',
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              15.0, 0, 5.0, 0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextCustom('Voice calls'),
                                              TextDesc('Outbound not working'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: SizedBox(
                                          height: 13,
                                          width: 25,
                                          child: Image.asset(
                                            'assets/app/icon_failed.png',
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  DividerCustomWrong(),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0.0, 1.0, 0.0, 1.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextDesc('Outbound'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            SizedBox(
                                              height: 13,
                                              width: 25,
                                              child: Image.asset(
                                                'assets/app/icon_failed.png',
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  DividerCustomWrong(),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0.0, 1.0, 0.0, 0.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextDesc('Inbound'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            SizedBox(
                                              height: 13,
                                              width: 25,
                                              child: Image.asset(
                                                'assets/app/icon_pass.png',
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SpaceCustom(),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(9.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: const Color(0xFF90A4AE),
                                  blurRadius: 3.0,
                                  offset: const Offset(0.0, 2.0),
                                )
                              ],
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        height: 45,
                                        child: SizedBox(
                                          child: Image.asset(
                                            'assets/app/icon_sms.png',
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              15.0, 0, 5.0, 0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextCustom('Texting'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        child: SizedBox(
                                          height: 13,
                                          width: 25,
                                          child: Image.asset(
                                            'assets/app/icon_pass.png',
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  DividerCustom(),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0.0, 1.0, 0.0, 1.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextDesc('Outbound'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            SizedBox(
                                              height: 13,
                                              width: 25,
                                              child: Image.asset(
                                                'assets/app/icon_pass.png',
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  DividerCustom(),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0.0, 1.0, 0.0, 0.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              TextDesc('Inbound'),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            SizedBox(
                                              height: 13,
                                              width: 25,
                                              child: Image.asset(
                                                'assets/app/icon_pass.png',
                                                fit: BoxFit.contain,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SpaceCustom(),
                      ],
                    ),
                  ),
                  Positioned(
                      left: 0,
                      top: 0,
                      child: new Container(
                        width: width,
                        child:
                            DeviceHeader(_userType, _model, _transactionCode),
                      )),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: ButtonBottom('Next', width, () {
                      Navigator.of(context).pushReplacementNamed(
                          PhysicalGradingScreen.routeName);
                    }),
                  )
                ],
              ),
      ),
    );
  }
}
