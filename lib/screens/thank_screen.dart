import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/login_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_screen_leading.dart';
import 'package:flutter/material.dart';

class ThankScreen extends StatelessWidget {
  static const routeName = '/thank';

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar();

    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressedLogin(context);
      },
      child: Scaffold(
          appBar: appBar,
          body: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          SizedBox(
                            width: 298,
                            child: Image.asset(
                              'assets/app/dingtoi_color.png',
                              fit: BoxFit.contain,
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18.0),
                            child: Image.asset(
                              'assets/app/box_shadow.png',
                              width: MediaQuery.of(context).size.width - 50,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextScreenLeading('Thank You'),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: const EdgeInsets.only(top: 169),
                        child: TextCustom(
                          'Let\'s scan your device',
                        ),
                      ),
                    ],
                  ),
                  SpaceCustom(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ButtonGradient(
                          'Scan Again',
                          173.0,
                          () => {
                                Navigator.of(context).pushReplacementNamed(
                                    LoginScreen.routeName),
                              }),
                    ],
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
