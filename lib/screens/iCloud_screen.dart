import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/phone_input_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';

class ICloudScreen extends StatefulWidget {
  static const routeName = '/iCloud';

  @override
  _ICloudScreenState createState() => _ICloudScreenState();
}

class _ICloudScreenState extends State<ICloudScreen> {
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    Functions.onInitICloud(context).then((value) {
      Future.delayed(const Duration(milliseconds: 4), () {
        setState(() {
          _isLoading = true;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: _isLoading
          ? Loading()
          : Container(
              child: Stack(
              children: <Widget>[
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: width - 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(9.0),
                          color: const Color(0xFFFBE7E7),
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xFF90A4AE),
                              blurRadius: 3.0,
                              offset: const Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 30),
                              Container(
                                child: SizedBox(
                                  height: 20,
                                  child: Image.asset(
                                    'assets/app/icon_failed.png',
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              TextCustom('iCloud Status: Lost/stolen'),
                              SizedBox(height: 30),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  child: ButtonBottom(
                      'Next',
                      width,
                      () => {
                            Navigator.of(context).pushReplacementNamed(
                                PhoneInputScreen.routeName),
                          }),
                ),
              ],
            )),
    );
  }
}
