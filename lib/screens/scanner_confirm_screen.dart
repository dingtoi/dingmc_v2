import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/thank_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/divider_custom.dart';
import 'package:dingtoimc/widgets/text_app_bar.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:flutter/material.dart';

class ScannerConfirmScreen extends StatefulWidget {
  static const routeName = '/scanner_confirm';

  @override
  _ScannerConfirmScreenState createState() => _ScannerConfirmScreenState();
}

class _ScannerConfirmScreenState extends State<ScannerConfirmScreen> {
  String _userType;

  @override
  void initState() {
    super.initState();
    Functions.onInitConfirmScreen(context).then((value) {
      setState(() {
        _userType = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final appBar = AppBar(
      title: TextAppBar('Dingtoi Scanner Report'),
    );

    return Scaffold(
      appBar: appBar,
      body: WillPopScope(
        onWillPop: () {
          return Functions.onBackPressedLogin(context);
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 57),
              child: ListView(
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Device name'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    TextDesc('Samsung Galaxy Prime J7'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Transaction Code'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    TextDesc('8nDWEVqPe'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc(
                                        'Phone\'s operating system - lastest version'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    TextDesc('10'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Phone\'s processor'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    TextDesc('EXYNOS7884B'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Phone\'s storage capacity'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    TextDesc('22.3GB of 32GB used'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Touchscreen'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Camera'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Flash'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Finger Scanner'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Face ID'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Speaker'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Volume'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_failed.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Wi-fi'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Bluetooth'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_failed.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Voice Outbound'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Voice Inbound'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_failed.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Text Outbound'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Text Inbound'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_failed.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('Blacklist'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                          Row(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    TextDesc('iCloud'),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: SizedBox(
                                        height: 10,
                                        child: Image.asset(
                                          'assets/app/icon_pass.png',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: ButtonBottom('Confirm', width, () {
                if (_userType == ConfigCustom().free ||
                    _userType == ConfigCustom().fee) {
                  Navigator.of(context)
                      .pushReplacementNamed(ThankScreen.routeName);
                }
              }),
            )
          ],
        ),
      ),
    );
  }
}
