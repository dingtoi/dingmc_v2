import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/phone_input.dart';
import 'package:dingtoimc/widgets/text_screen_title.dart';
import 'package:flutter/material.dart';

class PhoneInputScreen extends StatefulWidget {
  static const routeName = '/phone_input';

  @override
  _PhoneInputScreenState createState() => _PhoneInputScreenState();
}

class _PhoneInputScreenState extends State<PhoneInputScreen> {
  bool _isLoading = false;
  String _userType;

  @override
  void initState() {
    super.initState();
    Functions.onInitImeiInput(context).then((userType) {
      setState(() {
        _userType = userType;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressed(context);
      },
      child: Scaffold(
        appBar: AppBar(),
        body: _isLoading
            ? Loading()
            : Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextScreenTitle('Input'),
                            TextScreenTitle('Phone'),
                            TextScreenTitle('Number'),
                          ],
                        ),
                      ),
                      SizedBox(height: 70.0),
                      PhoneInputForm(_userType),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
