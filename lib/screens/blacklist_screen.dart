import 'dart:io' show Platform;

import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/iCloud_screen.dart';
import 'package:dingtoimc/screens/phone_input_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';

class BlacklistScreen extends StatefulWidget {
  static const routeName = '/blacklist';

  @override
  _BlacklistScreenState createState() => _BlacklistScreenState();
}

class _BlacklistScreenState extends State<BlacklistScreen> {
  double _long = 0.0;
  //double _lat = 0.0;

  @override
  void initState() {
    super.initState();
    Functions.onInitBlacklist(context).then((user) {
      setState(() {
        _long = user['long'];
        //_lat = user['lat'];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
          child: Stack(
        children: <Widget>[
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: width - 40,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(9.0),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0xFF90A4AE),
                        blurRadius: 3.0,
                        offset: const Offset(0.0, 2.0),
                      )
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30),
                        Container(
                          child: SizedBox(
                            height: 20,
                            child: !Functions.isEmpty(_long)
                                ? Image.asset(
                                    'assets/app/icon_pass.png',
                                  )
                                : Image.asset(
                                    'assets/app/icon_failed.png',
                                  ),
                          ),
                        ),
                        SizedBox(height: 20),
                        TextCustom(
                            'Blacklist Status: ${!Functions.isEmpty(_long) ? 'Clean' : 'Not Verified'}'),
                        SizedBox(height: 30),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: ButtonBottom(
                'Next',
                width,
                () => {
                      if (Platform.isAndroid)
                        {
                          Navigator.of(context)
                              .pushReplacementNamed(PhoneInputScreen.routeName),
                        }
                      else if (Platform.isIOS)
                        {
                          Navigator.of(context)
                              .pushReplacementNamed(ICloudScreen.routeName),
                        }
                    }),
          ),
        ],
      )),
    );
  }
}
