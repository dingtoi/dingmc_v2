import 'dart:async' show StreamSubscription;

import 'dart:io' show Platform;
import 'dart:typed_data';
import 'package:connectivity/connectivity.dart';
import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/screens/blacklist_screen.dart';
import 'package:dingtoimc/screens/imei_input_screen.dart';
import 'package:dingtoimc/screens/login_screen.dart';
import 'package:dingtoimc/screens/thank_screen.dart';
import 'package:dingtoimc/widgets/device_header.dart';
import 'package:dingtoimc/widgets/divider_custom.dart';
import 'package:dingtoimc/widgets/divider_custom_wrong.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/over_repaint_boundary.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_app_bar.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import 'dart:ui' as ui;

class ScannerBasicScreen extends StatefulWidget {
  static const routeName = '/scanner_basic';

  @override
  _ScannerBasicScreenState createState() => _ScannerBasicScreenState();
}

class _ScannerBasicScreenState extends State<ScannerBasicScreen> {
  bool _isInternet = true;
  StreamSubscription _subscription;

  String _userType = '';
  bool _isLoading = false;
  bool _isWifi = false;
  bool _isBluetooth = false;
  bool _isCamera = false;
  bool _isFlash = false;
  bool _isFingerprint = false;
  bool _isFaceID = false;
  String _freeSpace = '';
  String _totalSpace = '';
  String _model = '';
  String _version = '';
  String _hardware = '';
  String _transactionCode = '';
  String _imei = '';
  GlobalKey globalKey = GlobalKey();

  Future _shareFileImage() async {
    try {
      var renderObject = globalKey.currentContext.findRenderObject();
      RenderRepaintBoundary boundary = renderObject;
      setState(() {
        _isLoading = true;
      });
      ui.Image captureImage = await boundary.toImage(pixelRatio: 5.0);
      ByteData byteData =
          await captureImage.toByteData(format: ui.ImageByteFormat.png);
      Uint8List pngBytes = byteData.buffer.asUint8List();
      await Share.file('Dingtoi MC', 'dingtoimc.png', pngBytes, 'image/png',
          text: 'Dingtoi MC.');
      setState(() {
        _isLoading = false;
      });
    } catch (error) {
      print('error $error');
      Functions.onConfirmSuccess(
          context, 'Gallery', 'Something Wrong ... Please Try Again');
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future _saveFileImage() async {
    PermissionStatus status = await Permission.storage.status;
    if (status.isUndetermined || status.isRestricted || status.isDenied) {
      if (await Permission.storage.request().isGranted) {
        try {
          var renderObject = globalKey.currentContext.findRenderObject();
          RenderRepaintBoundary boundary = renderObject;
          setState(() {
            _isLoading = true;
          });
          ui.Image captureImage = await boundary.toImage(pixelRatio: 5.0);
          ByteData byteData =
              await captureImage.toByteData(format: ui.ImageByteFormat.png);
          Uint8List pngBytes = byteData.buffer.asUint8List();
          ImageGallerySaver.saveImage(pngBytes).then((result) {
            Functions.onConfirmSuccess(context, 'Gallery',
                'Save Image to Gallery success. You can check image on your library');
            setState(() {
              _isLoading = false;
            });
          });
        } catch (error) {
          print('error $error');
          Functions.onConfirmSuccess(
              context, 'Gallery', 'Something Wrong ... Please Try Again');
          setState(() {
            _isLoading = false;
          });
        }
      }
    } else if (status.isPermanentlyDenied) {
      openAppSettings();
    } else if (status.isGranted) {
      try {
        var renderObject = globalKey.currentContext.findRenderObject();
        RenderRepaintBoundary boundary = renderObject;
        setState(() {
          _isLoading = true;
        });
        ui.Image captureImage = await boundary.toImage(pixelRatio: 5.0);
        ByteData byteData =
            await captureImage.toByteData(format: ui.ImageByteFormat.png);
        Uint8List pngBytes = byteData.buffer.asUint8List();
        ImageGallerySaver.saveImage(pngBytes).then((result) {
          Functions.onConfirmSuccess(context, 'Gallery',
              'Save Image to Gallery success. You can check image on your library');
          setState(() {
            _isLoading = false;
          });
        });
      } catch (error) {
        print('error $error');
        Functions.onConfirmSuccess(
            context, 'Gallery', 'Something Wrong ... Please Try Again');
        setState(() {
          _isLoading = false;
        });
      }
    }
  }

  Future _initPlatform() async {
    String model = '';
    String version = '';
    String hardware = '';
    List cameras;
    bool isFaceID = false;
    bool isFingerprint = false;
    bool isWifi = false;
    bool isBluetooth = false;
    bool isFlash = false;

    if (!mounted) return;

    setState(() {
      _isLoading = true;
    });

    Map user = await Functions.onInitDeviceScanner(context);
    setState(() {
      _userType = user['userType'];
      _transactionCode = user['transactionCode'];
      _imei = user['imei'];
    });
    cameras = await Functions.checkCamera();
    isFlash = await Functions.checkFlash();
    isFaceID = await Functions.checkBiometricsFace();
    isFingerprint = await Functions.checkBiometricsFingerprint();
    isWifi = await Functions.checkWifi();
    isBluetooth = await Functions.checkBluetooth();
    Map diskSpace = await Functions.getDiskSpace();

    if (Platform.isAndroid) {
      Map dataAndroid = await Functions.getDeviceDataAndroid();
      final device = Provider.of<Device>(context, listen: false);
      try {
        model = await device.getDeviceNameAndroid(dataAndroid['model']);
        if (Functions.isEmpty(model)) model = dataAndroid['model'];
      } catch (error) {
        model = dataAndroid['model'];
      }

      version = dataAndroid['release'];
      hardware = dataAndroid['hardware'];
    } else if (Platform.isIOS) {
      Map dataIOS = await Functions.getDeviceDataIOS();
      Map deviceInfo = Functions.getDeviceInfoIOS(dataIOS['machine']);

      model = deviceInfo['name'];
      version = dataIOS['version'];
      hardware = deviceInfo['processor'];
    }

    setState(() {
      _isWifi = isWifi;
      _isBluetooth = isBluetooth;
      _freeSpace = diskSpace['free'];
      _totalSpace = diskSpace['total'];
      _isLoading = false;
      _model = model;
      _version = version;
      _hardware = hardware;
      _isCamera = !Functions.isEmpty(cameras.length) ? true : false;
      _isFlash = isFlash;
      _isFingerprint = isFingerprint;
      _isFaceID = isFaceID;
    });
  }

  @override
  void initState() {
    super.initState();
    _initPlatform();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        Functions.onAlertConnectivity(context);
        setState(() {
          _isInternet = false;
        });
      } else {
        if (!_isInternet) Navigator.of(context).pop(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (!Functions.isEmpty(_subscription)) _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final appBar = AppBar(
      title: TextAppBar('Dingtoi Scanner Report'),
      automaticallyImplyLeading: false,
    );
    return Scaffold(
      appBar: appBar,
      body: WillPopScope(
        onWillPop: () {
          return Functions.onBackPressedLogin(context);
        },
        child: _isLoading
            ? Loading()
            : Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 127.0, 0, 57),
                    child: SingleChildScrollView(
                      child: OverRepaintBoundary(
                        key: globalKey,
                        child: RepaintBoundary(
                          child: Container(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_hardware.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom(
                                                          'Hardware verification'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: Image.asset(
                                                    'assets/app/icon_pass.png',
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          DividerCustom(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 1.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc(
                                                          'Phone operating system - lastest version'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    TextDesc(_version),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          DividerCustom(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 1.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc(
                                                          'Phone processor'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    TextDesc(_hardware),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          DividerCustom(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 0.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc(
                                                          'Storage capacity'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    TextDesc(
                                                        '$_freeSpace of $_totalSpace used'),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  color: const Color(0xFFF6F6F6),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20.0, 15, 20.0, 15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        TextCustom('GENERAL'),
                                      ],
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_touchscreen.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom(
                                                          'Touch screen'),
                                                      TextDesc(
                                                          'Touch screen working'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: Image.asset(
                                                    'assets/app/icon_pass.png',
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: _isCamera && _isFlash
                                          ? Colors.white
                                          : const Color(0xFFFBE7E7),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_camera.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom(
                                                          'Camera + Flash'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: _isCamera && _isFlash
                                                      ? Image.asset(
                                                          'assets/app/icon_pass.png',
                                                        )
                                                      : Image.asset(
                                                          'assets/app/icon_failed.png',
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          _isCamera && _isFlash
                                              ? DividerCustom()
                                              : DividerCustomWrong(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 1.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc('Camera'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 13,
                                                      width: 25,
                                                      child: _isCamera
                                                          ? Image.asset(
                                                              'assets/app/icon_pass.png',
                                                              fit: BoxFit
                                                                  .contain,
                                                            )
                                                          : Image.asset(
                                                              'assets/app/icon_failed.png',
                                                              fit: BoxFit
                                                                  .contain,
                                                            ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          _isCamera && _isFlash
                                              ? DividerCustom()
                                              : DividerCustomWrong(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 0.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc('Flash'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 13,
                                                      width: 25,
                                                      child: _isFlash
                                                          ? Image.asset(
                                                              'assets/app/icon_pass.png',
                                                              fit: BoxFit
                                                                  .contain,
                                                            )
                                                          : Image.asset(
                                                              'assets/app/icon_failed.png',
                                                              fit: BoxFit
                                                                  .contain,
                                                            ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_volume.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom(
                                                          'Volume Adjustment'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: Image.asset(
                                                    'assets/app/icon_pass.png',
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          DividerCustom(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 1.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc('Volume'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      width: 25,
                                                      height: 13,
                                                      child: Image.asset(
                                                        'assets/app/icon_pass.png',
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                          DividerCustom(),
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0.0, 1.0, 0.0, 0.0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextDesc('Speaker'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: <Widget>[
                                                    SizedBox(
                                                      height: 13,
                                                      width: 25,
                                                      child: Image.asset(
                                                        'assets/app/icon_pass.png',
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: _isFingerprint
                                          ? Colors.white
                                          : const Color(0xFFFBE7E7),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_fingerprint.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom(
                                                          'Finger Scanner'),
                                                      _isFingerprint
                                                          ? TextDesc(
                                                              'Finger Scanner working')
                                                          : TextDesc(
                                                              'Finger Scanner not working'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: _isFingerprint
                                                      ? Image.asset(
                                                          'assets/app/icon_pass.png',
                                                        )
                                                      : Image.asset(
                                                          'assets/app/icon_failed.png',
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: _isFaceID
                                          ? Colors.white
                                          : const Color(0xFFFBE7E7),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_faceid.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom('Face ID'),
                                                      _isFaceID
                                                          ? TextDesc(
                                                              'Face ID working')
                                                          : TextDesc(
                                                              'Face ID not working'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: _isFaceID
                                                      ? Image.asset(
                                                          'assets/app/icon_pass.png',
                                                        )
                                                      : Image.asset(
                                                          'assets/app/icon_failed.png',
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Container(
                                  width: MediaQuery.of(context).size.width,
                                  color: const Color(0xFFF6F6F6),
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20.0, 15, 20.0, 15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        TextCustom('NETWORK'),
                                      ],
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: _isWifi
                                          ? Colors.white
                                          : const Color(0xFFFBE7E7),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_wifi.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom('Wi-fi'),
                                                      _isWifi
                                                          ? TextDesc(
                                                              'Wi-fi working')
                                                          : TextDesc(
                                                              'Wi-fi not working'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: _isWifi
                                                      ? Image.asset(
                                                          'assets/app/icon_pass.png',
                                                        )
                                                      : Image.asset(
                                                          'assets/app/icon_failed.png',
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                                Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(9.0),
                                      color: _isBluetooth
                                          ? Colors.white
                                          : const Color(0xFFFBE7E7),
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xFF90A4AE),
                                          blurRadius: 3.0,
                                          offset: const Offset(0.0, 2.0),
                                        )
                                      ],
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Container(
                                                height: 45,
                                                child: SizedBox(
                                                  child: Image.asset(
                                                    'assets/app/icon_bluetooth.png',
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          15.0, 0, 5.0, 0),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      TextCustom('Bluetooth'),
                                                      _isBluetooth
                                                          ? TextDesc(
                                                              'Bluetooth working')
                                                          : TextDesc(
                                                              'Bluetooth not working'),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: SizedBox(
                                                  height: 13,
                                                  width: 25,
                                                  child: _isBluetooth
                                                      ? Image.asset(
                                                          'assets/app/icon_pass.png',
                                                        )
                                                      : Image.asset(
                                                          'assets/app/icon_failed.png',
                                                        ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                SpaceCustom(),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      left: 0,
                      top: 0,
                      child: new Container(
                        width: width,
                        child:
                            DeviceHeader(_userType, _model, _transactionCode),
                      )),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: Container(
                      color: Color(0xFF303A96),
                      width: MediaQuery.of(context).size.width,
                      height: 57,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                _shareFileImage();
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.screen_share,
                                      color: Colors.white, size: 24.0),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text(
                                    'Share',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                if (_userType == ConfigCustom().free ||
                                    _userType == ConfigCustom().fee) {
                                  if (Functions.isEmpty(_transactionCode)) {
                                    Navigator.of(context).pushReplacementNamed(
                                        ImeiInputScreen.routeName);
                                  } else {
                                    final device = Provider.of<Device>(context,
                                        listen: false);
                                    device.setImei(_imei).then((_) {
                                      Functions.onConfirmShareLocation(
                                          context,
                                          'Share Location',
                                          'Do you want to allow share location to check blacklist',
                                          () {
                                        Functions.getLocation()
                                            .then((location) {
                                          device
                                              .setLocation(location.longitude,
                                                  location.latitude)
                                              .then((_) {
                                            Navigator.of(context)
                                                .pushReplacementNamed(
                                                    BlacklistScreen.routeName);
                                          });
                                        });
                                      }, () {
                                        Navigator.of(context)
                                            .pushReplacementNamed(
                                                ThankScreen.routeName);
                                      });
                                    });
                                  }
                                } else if (_userType ==
                                    ConfigCustom().anonymous) {
                                  Navigator.of(context).pushReplacementNamed(
                                      LoginScreen.routeName);
                                }
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.open_in_browser,
                                      color: Colors.white, size: 24.0),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text(
                                    _userType == ConfigCustom().anonymous
                                        ? 'Scan Again'
                                        : 'Scan',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                _saveFileImage();
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.image,
                                      color: Colors.white, size: 24.0),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text(
                                    'Save',
                                    style: TextStyle(
                                        fontSize: 12, color: Colors.white),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
      ),
    );
  }
}
