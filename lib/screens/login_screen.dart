import 'package:connectivity/connectivity.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/user.dart';
import 'package:dingtoimc/screens/welcome_start_screen.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/login_form.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'dart:async' show StreamSubscription;

class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isLoading = false;
  bool _isInternet = true;
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    Functions.onInitLoginScreen(context).then((_) {
      setState(() {
        _isLoading = true;
      });
      Future.delayed(Duration(milliseconds: 500)).then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        setState(() {
          _isInternet = false;
        });
        Functions.onAlertConnectivity(context);
      } else {
        if (!_isInternet) Navigator.of(context).pop(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (!Functions.isEmpty(_subscription)) _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context, listen: false);

    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressed(context);
      },
      child: Scaffold(
        appBar: null,
        body: _isLoading
            ? Loading()
            : Center(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(35.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 248,
                          child: Image.asset(
                            'assets/app/dingtoi_color.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(height: 70.0),
                        LoginForm(),
                        SpaceCustom(),
                        InkWell(
                          child: Container(
                            height: 30,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                TextCustom('Skip Login To Scan Anonymous'),
                              ],
                            ),
                          ),
                          onTap: () {
                            user.setUserAnonymous().then((_) {
                              Navigator.of(context).pushReplacementNamed(
                                WelcomeStartScreen.routeName,
                              );
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
