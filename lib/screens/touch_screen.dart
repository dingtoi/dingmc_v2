import 'dart:async' show StreamSubscription;

import 'package:connectivity/connectivity.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_app_bar.dart';
import 'package:dingtoimc/widgets/touch_screen.dart';
import 'package:flutter/material.dart';

class TouchScreen extends StatefulWidget {
  static const routeName = '/touch_screen';

  @override
  _TouchScreenState createState() => _TouchScreenState();
}

class _TouchScreenState extends State<TouchScreen> {
  bool _isInternet = true;
  bool _isTouch = false;
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    Functions.onInitTouchscreen(context).then((value) {});
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        Functions.onAlertConnectivity(context);
        setState(() {
          _isInternet = false;
        });
      } else {
        if (!_isInternet) Navigator.of(context).pop(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (!Functions.isEmpty(_subscription)) _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressedLogin(context);
      },
      child: Scaffold(
        appBar: null,
        body: _isTouch
            ? Stack(
                children: <Widget>[
                  TouchScreenCustom(),
                ],
              )
            : GestureDetector(
                onTapDown: (_) {
                  setState(() {
                    _isTouch = true;
                  });
                },
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.transparent,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 60,
                          child: Image.asset(
                            'assets/app/icon_touch.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                        SpaceCustom(),
                        SpaceCustom(),
                        Container(
                          width: 228,
                          child: TextAppBar(
                              'Touch the icon and color the entire screen to blue'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
