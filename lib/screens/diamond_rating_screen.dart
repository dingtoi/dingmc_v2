import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/scanner_confirm_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/divider_custom.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:dingtoimc/widgets/text_screen_title.dart';
import 'package:flutter/material.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DiamondRatingScreen extends StatefulWidget {
  static const routeName = '/diamond_rating';

  @override
  _DiamondRatingScreenState createState() => _DiamondRatingScreenState();
}

class _DiamondRatingScreenState extends State<DiamondRatingScreen> {
  @override
  void initState() {
    super.initState();
    Functions.onInitDiamondRating(context).then((user) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 30.0, 0, 57.0),
              child: ListView(
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextScreenTitle('Dingtoi'),
                          TextScreenTitle('Diamond'),
                          TextScreenTitle('Rating'),
                        ],
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                child: SizedBox(
                                  width: 93,
                                  child: Image.asset(
                                    'assets/app/phone.png',
                                    fit: BoxFit.contain,
                                  ),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.fromLTRB(5.0, 0, 5.0, 0),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      TextDesc('Device Name'),
                                      SizedBox(height: 4),
                                      TextCustom('Test Device'),
                                      DividerCustom(),
                                      TextDesc('Transaction Code'),
                                      SizedBox(height: 4),
                                      TextCustom('8nDWEVqPe'),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          DividerCustom(),
                        ],
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: width - 40,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(9.0),
                                color: Colors.white,
                                boxShadow: [
                                  BoxShadow(
                                    color: const Color(0xFF90A4AE),
                                    blurRadius: 3.0,
                                    offset: const Offset(0.0, 2.0),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(20, 0, 20, 0),
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 30),
                                    SmoothStarRating(
                                        allowHalfRating: false,
                                        onRated: (v) {},
                                        starCount: 5,
                                        rating: 3,
                                        size: 40.0,
                                        isReadOnly: true,
                                        filledIconData: Icons.star,
                                        halfFilledIconData: Icons.star_half,
                                        color: Colors.amber,
                                        borderColor: Colors.amber,
                                        spacing: 0.0),
                                    SizedBox(height: 20),
                                    TextCustom(
                                        'Very good phone with no major functional loss.'),
                                    SizedBox(height: 30),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: ButtonBottom('Next', width, () {
                Navigator.of(context)
                    .pushReplacementNamed(ScannerConfirmScreen.routeName);
              }),
            )
          ],
        ),
      ),
    );
  }
}
