import 'dart:async' show StreamSubscription;

import 'package:connectivity/connectivity.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/screens/touch_screen.dart';
import 'package:dingtoimc/widgets/button_gradient.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_screen_leading.dart';
import 'package:flutter/material.dart';

class WelcomeStartScreen extends StatefulWidget {
  static const routeName = '/welcome_start';

  @override
  _WelcomeStartScreenState createState() => _WelcomeStartScreenState();
}

class _WelcomeStartScreenState extends State<WelcomeStartScreen> {
  bool _isInternet = true;
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    Functions.onInitWelcomeScreen(context).then((value) {});
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.none) {
        Functions.onAlertConnectivity(context);
        setState(() {
          _isInternet = false;
        });
      } else {
        if (!_isInternet) Navigator.of(context).pop(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    if (!Functions.isEmpty(_subscription)) _subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressedLogin(context);
      },
      child: Scaffold(
          appBar: null,
          body: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          SizedBox(
                            width: 298,
                            child: Image.asset(
                              'assets/app/dingtoi_color.png',
                              fit: BoxFit.contain,
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 18.0),
                            child: Image.asset(
                              'assets/app/box_shadow.png',
                              width: MediaQuery.of(context).size.width - 50,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SpaceCustom(),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextScreenLeading('Let\'s Scan'),
                      TextScreenLeading('Your Device'),
                    ],
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                  SpaceCustom(),
                  SpaceCustom(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ButtonGradient(
                          'Scan',
                          173.0,
                          () => {
                                Navigator.of(context)
                                    .pushReplacementNamed(TouchScreen.routeName)
                              }),
                    ],
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
