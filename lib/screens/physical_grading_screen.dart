import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/device.dart';
import 'package:dingtoimc/screens/diamond_rating_screen.dart';
import 'package:dingtoimc/widgets/button_bottom.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_desc.dart';
import 'package:dingtoimc/widgets/text_screen_title.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PhysicalGradingScreen extends StatefulWidget {
  static const routeName = '/physical_grading';

  @override
  _PhysicalGradingScreenState createState() => _PhysicalGradingScreenState();
}

class _PhysicalGradingScreenState extends State<PhysicalGradingScreen> {
  String _selectedGrade = 'A';
  String _transactionType = '';

  @override
  void initState() {
    super.initState();
    Functions.onInitPhysicalGrading(context).then((user) {
      setState(() {
        _transactionType = user['transactionType'];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final device = Provider.of<Device>(context, listen: false);

    String _byUser = 'By User';
    if (_transactionType == ConfigCustom().originator ||
        _transactionType == ConfigCustom().recipient) _byUser = 'By Originator';

    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 30.0, 0, 57.0),
              child: ListView(
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          TextScreenTitle('Physical'),
                          TextScreenTitle('Grading done'),
                          TextScreenTitle(_byUser),
                        ],
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedGrade = 'A';
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(9.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xFF90A4AE),
                              blurRadius: 3.0,
                              offset: const Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: 25,
                                    child: SizedBox(
                                      child: _selectedGrade == 'A'
                                          ? Image.asset(
                                              'assets/app/icon_check.png',
                                            )
                                          : Image.asset(
                                              'assets/app/icon_no_check.png',
                                            ),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15.0, 0, 5.0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextCustom('GRADE A'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0.0, 10.0, 0.0, 1.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextDesc('Almost new'),
                                          SizedBox(
                                            height: 2.0,
                                          ),
                                          TextDesc(
                                              'Very few or imperceptible scratches'),
                                          SizedBox(
                                            height: 2.0,
                                          ),
                                          TextDesc('Very minimal usage'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedGrade = 'B';
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(9.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xFF90A4AE),
                              blurRadius: 3.0,
                              offset: const Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: 25,
                                    child: SizedBox(
                                      child: _selectedGrade == 'B'
                                          ? Image.asset(
                                              'assets/app/icon_check.png',
                                            )
                                          : Image.asset(
                                              'assets/app/icon_no_check.png',
                                            ),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15.0, 0, 5.0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextCustom('GRADE B'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0.0, 10.0, 0.0, 1.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextDesc(
                                              'Almost Grade A but a few more visible'),
                                          SizedBox(
                                            height: 2.0,
                                          ),
                                          TextDesc('minior scratches'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedGrade = 'C';
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(9.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xFF90A4AE),
                              blurRadius: 3.0,
                              offset: const Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: 25,
                                    child: SizedBox(
                                      child: _selectedGrade == 'C'
                                          ? Image.asset(
                                              'assets/app/icon_check.png',
                                            )
                                          : Image.asset(
                                              'assets/app/icon_no_check.png',
                                            ),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15.0, 0, 5.0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextCustom('GRADE C'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0.0, 10.0, 0.0, 1.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextDesc(
                                              'Obvious signs of visible wear'),
                                          SizedBox(
                                            height: 2.0,
                                          ),
                                          TextDesc(
                                              'Many scratches, but no dents or cracks anywhere'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _selectedGrade = 'D';
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(9.0),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: const Color(0xFF90A4AE),
                              blurRadius: 3.0,
                              offset: const Offset(0.0, 2.0),
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    height: 25,
                                    child: SizedBox(
                                      child: _selectedGrade == 'D'
                                          ? Image.asset(
                                              'assets/app/icon_check.png',
                                            )
                                          : Image.asset(
                                              'assets/app/icon_no_check.png',
                                            ),
                                    ),
                                  ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          15.0, 0, 5.0, 0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextCustom('GRADE D'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          0.0, 10.0, 0.0, 1.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          TextDesc(
                                              'Visible cracks on screen and/or casing dents'),
                                          SizedBox(
                                            height: 2.0,
                                          ),
                                          TextDesc('Deep scratches and scuffs'),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SpaceCustom(),
                  SpaceCustom(),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: ButtonBottom(
                  'Next',
                  width,
                  () => {
                        device.setSelectedGrade(_selectedGrade).then((_) {
                          Navigator.of(context).pushReplacementNamed(
                              DiamondRatingScreen.routeName);
                        }),
                      }),
            )
          ],
        ),
      ),
    );
  }
}
