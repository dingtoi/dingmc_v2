import 'package:dingtoimc/helpers/functions.dart';
import 'package:dingtoimc/providers/user.dart';
import 'package:dingtoimc/screens/login_screen.dart';
import 'package:dingtoimc/screens/welcome_start_screen.dart';
import 'package:dingtoimc/widgets/loading.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:dingtoimc/widgets/text_screen_title.dart';
import 'package:dingtoimc/widgets/transaction_check_form.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransactionCodeScreen extends StatefulWidget {
  static const routeName = '/transaction_code';

  @override
  _TransactionCodeScreenState createState() => _TransactionCodeScreenState();
}

class _TransactionCodeScreenState extends State<TransactionCodeScreen> {
  String _userType = '';
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    Functions.onInitTransactionCode(context).then((value) {
      setState(() {
        _userType = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context, listen: false);

    final appBar = AppBar(
      actions: <Widget>[
        _userType == 'free' || _userType == 'fee'
            ? IconButton(
                icon: Icon(Icons.input),
                color: const Color(0xFF303A96),
                onPressed: () {
                  Functions.onConfirmYesNo(context, 'Do you want to log out?',
                      'Log out your application', () {
                    setState(() {
                      _isLoading = true;
                    });
                    SharedPreferences.getInstance().then((prefs) {
                      String token = prefs.getString('token');
                      user.logout(token).then((_) {
                        user.setLogout().then((_) => Navigator.of(context)
                            .pushReplacementNamed(LoginScreen.routeName));
                        _isLoading = false;
                      });
                    });
                  });
                },
              )
            : Center(),
      ],
    );

    return WillPopScope(
      onWillPop: () {
        return Functions.onBackPressed(context);
      },
      child: Scaffold(
        appBar: _isLoading ? null : appBar,
        body: _isLoading
            ? Loading()
            : Container(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextScreenTitle('Input'),
                            TextScreenTitle('Transaction'),
                            TextScreenTitle('Code'),
                          ],
                        ),
                      ),
                      SizedBox(height: 70.0),
                      TransactionCheckForm(),
                      SizedBox(height: 26),
                      Center(
                        child: GestureDetector(
                          child: TextCustom('Skip Transaction Code For Now'),
                          onTap: () {
                            Navigator.of(context).pushReplacementNamed(
                              WelcomeStartScreen.routeName,
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
