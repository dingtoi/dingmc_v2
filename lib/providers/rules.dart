import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Rules with ChangeNotifier {
  var _rules = {};

  get rules {
    return _rules;
  }

  Future getRules() async {
    const url = 'http://dingtoi.com:30080/api/v2/transaction/rules';

    try {
      var result = await http.get(
        url,
        //body: json.encode({}),
      );
      return result;
    } catch (error) {
      throw new FormatException(error);
    }
  }
}
