import 'package:dingtoimc/helpers/api.dart';
import 'package:dingtoimc/helpers/config.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Transaction with ChangeNotifier {
  String _imei = '';
  String _grade = '';
  String _transactionCode = '';
  String _transactionType = '';

  String get getIMEI {
    return _imei;
  }

  String get getGrade {
    return _grade;
  }

  String get getTransactionCode {
    return _transactionCode;
  }

  String get getTransactionType {
    return _transactionType;
  }

  Future checkTransactionCode(code, userId, token) async {
    final url = '${Api().prefix}/${Api().version}/transaction/check';

    final Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $token',
    };

    try {
      var result = await http.post(
        url,
        body: {'code': code, 'userId': userId},
        headers: requestHeaders,
      );
      return result;
    } catch (error) {
      throw new FormatException(error);
    }
  }

  Future setTransactionCode(transactionCode) async {
    _transactionCode = transactionCode;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('transactionCode', transactionCode);
    return Future.value('done');
  }

  Future setTransactionType(transactionType) async {
    //_transactionType = transactionType == 1 ? '';
    _transactionType = transactionType == 1
        ? ConfigCustom().recipient
        : ConfigCustom().originator;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('transactionType', _transactionType);
    return Future.value('done');
  }
}
