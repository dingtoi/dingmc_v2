import 'package:dingtoimc/helpers/api.dart';
import 'package:dingtoimc/helpers/config.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class User with ChangeNotifier {
  String _type = ConfigCustom().none;
  String _token = '';
  String _id = '';
  String _email = '';

  String get getType {
    return _type;
  }

  String get getToken {
    return _token;
  }

  String get getId {
    return _id;
  }

  String get getEmail {
    return _email;
  }

  Future login(email, pass) async {
    final url = '${Api().prefix}/oauth/token';

    final Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ZGluZ3RvaTpzZWNyZXRzaXRl',
    };

    try {
      var result = await http.post(
        url,
        body: {
          'grant_type': 'password',
          'username': email,
          'password': pass,
        },
        headers: requestHeaders,
      );
      return result;
    } catch (error) {
      throw new FormatException(error);
    }
  }

  Future logout(token) async {
    final url = '${Api().prefix}/${Api().preVersion}/user/logout';

    final Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Authorization': 'Bearer $token',
    };

    try {
      var result = await http.post(
        url,
        body: {},
        headers: requestHeaders,
      );
      return result;
    } catch (error) {
      throw new FormatException(error);
    }
  }

  Future setLogout() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    _type = 'none';
    _token = '';
    _id = '';
    _email = '';
    return Future.value('done');
  }

  Future setUserAnonymous() async {
    _type = ConfigCustom().anonymous;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('userType', ConfigCustom().anonymous);
    notifyListeners();
    return Future.value('done');
  }

  Future setUser(user) async {
    _type = user['user_type'] == 1 ? ConfigCustom().free : ConfigCustom().fee;
    _token = user['accessToken'];
    _id = user['user']['id'];
    _email = user['user']['email'];
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('userType', _type);
    prefs.setString('token', _token);
    prefs.setString('email', _email);
    prefs.setString('userId', _id);

    notifyListeners();
    return Future.value('done');
  }
}
