import 'dart:convert';

import 'package:dingtoimc/helpers/api.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Device with ChangeNotifier {
  String _imei = '';
  String _grade = '';
  double _longtitude = 0.0;
  double _latitude = 0.0;

  String get getIMEI {
    return _imei;
  }

  String get getGrade {
    return _grade;
  }

  double get getLongtitude {
    return _longtitude;
  }

  double get getLatitude {
    return _latitude;
  }

  Future checkImei(imei, token) async {
    final url = '${Api().prefix}/${Api().version}/device/checkImei';

    final Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $token',
    };

    try {
      var result = await http.post(
        url,
        body: {'imei': imei},
        headers: requestHeaders,
      );
      return result;
    } catch (error) {
      throw new FormatException(error);
    }
  }

  Future setImei(imei) async {
    _imei = imei;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('imei', imei);
    notifyListeners();
    return Future.value('done');
  }

  Future setSelectedGrade(grade) async {
    _grade = grade;
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('grade', grade);
    return Future.value('done');
  }

  Future<String> getDeviceNameAndroid(model) async {
    final url = '${Api().prefixDeviceList}/api/android/device/list';

    try {
      var result = await http.post(
        url,
        body: {'model': model},
      );
      List list = json.decode(result.body)['list'];
      if (list.length > 0)
        return list[0]['name'];
      else
        return '';
    } catch (error) {
      throw new FormatException(error);
    }
  }

  Future setLocation(longtitude, latitude) async {
    _longtitude = longtitude;
    _latitude = latitude;
    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble('long', longtitude);
    prefs.setDouble('lat', latitude);
    return Future.value('done');
  }
}
