class ConfigCustom {
  String none = 'none';
  String anonymous = 'anonymous';
  String free = 'free';
  String fee = 'fee';
  String transaction = 'transaction';
  String originator = 'originator';
  String recipient = 'recipient';
  int touchscreenRow = 6;
  int touchscreenCol = 7;
}
