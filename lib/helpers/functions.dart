import 'dart:async' show StreamSubscription;

import 'package:camera/camera.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dingtoimc/helpers/config.dart';
import 'package:dingtoimc/screens/login_screen.dart';
import 'package:dingtoimc/screens/transaction_code_screen.dart';
import 'package:dingtoimc/widgets/button_small.dart';
import 'package:dingtoimc/widgets/space_custom.dart';
import 'package:dingtoimc/widgets/text_custom.dart';
import 'package:disk_space/disk_space.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_torch/flutter_torch.dart';
import 'package:geolocator/geolocator.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:device_info/device_info.dart';

class Functions {
  static Future<StreamSubscription> streamConnectivity(
      context, callbackYes, callbackNo) async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult != ConnectivityResult.none) {}
      return Connectivity()
          .onConnectivityChanged
          .listen((ConnectivityResult result) {
        if (result == ConnectivityResult.none) {
          callbackYes();
          Functions.onAlertConnectivity(context);
        } else {
          callbackNo();
        }
      });
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value(null);
    }
  }

  static Future<bool> checkBluetooth() async {
    try {
      FlutterBlue flutterBlue = FlutterBlue.instance;
      return await flutterBlue.isAvailable;
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value(false);
    }
  }

  static Future<bool> checkWifi() async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult != ConnectivityResult.none)
        return Future.value(true);
      else
        return Future.value(false);
    } on PlatformException catch (error) {
      print('error $error');
      return Future.error(false);
    }
  }

  static Future<bool> checkBiometricsFace() async {
    try {
      var localAuth = LocalAuthentication();
      bool canCheckBiometrics = await localAuth.canCheckBiometrics;
      if (!canCheckBiometrics)
        return Future.value(false);
      else {
        List<BiometricType> availableBiometrics =
            await localAuth.getAvailableBiometrics();
        return Future.value(availableBiometrics.contains(BiometricType.face));
      }
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value(false);
    }
  }

  static Future<bool> checkBiometricsFingerprint() async {
    try {
      var localAuth = LocalAuthentication();
      bool canCheckBiometrics = await localAuth.canCheckBiometrics;
      if (!canCheckBiometrics)
        return Future.value(false);
      else {
        List<BiometricType> availableBiometrics =
            await localAuth.getAvailableBiometrics();
        return Future.value(
            availableBiometrics.contains(BiometricType.fingerprint));
      }
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value(false);
    }
  }

  static Future<Position> getLocation() async {
    try {
      Geolocator geolocator = Geolocator();
      return await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      /*if (await Permission.locationWhenInUse.serviceStatus.isEnabled) {
        if (await Permission.locationAlways.request().isGranted) {
          GeolocationStatus geolocationStatus =
              await geolocator.checkGeolocationPermissionStatus();
          print('assas $geolocationStatus isShowsn');
          return await geolocator.getCurrentPosition(
              desiredAccuracy: LocationAccuracy.high);
        } else {
          await Permission.locationWhenInUse.request();
          return Future.value('not_granted');
        }
      } else {
        return Future.value('not_enable');
      }*/
    } on PlatformException catch (error) {
      return Future.error(error);
    }
  }

  static Future checkVolume() async {
    try {
      return await Future.value('');
    } on PlatformException catch (error) {
      return Future.error(error);
    }
  }

  static Future<List> checkCamera() async {
    try {
      return await availableCameras();
    } on PlatformException catch (error) {
      print('error: $error');
      return Future.value([]);
    }
  }

  static Future<bool> checkFlash() async {
    try {
      return await FlutterTorch.hasLamp;
    } on PlatformException catch (error) {
      print('error: $error');
      return Future.value(false);
    }
  }

  static String formatMB(double number) {
    return '${(number / 1024).round()} GB';
  }

  static Future<Map> getDiskSpace() async {
    try {
      double free = await DiskSpace.getFreeDiskSpace;
      double total = await DiskSpace.getTotalDiskSpace;

      double used = total - free;

      double totalGB = total / 1024;

      if (totalGB <= 8)
        totalGB = 8;
      else if (totalGB > 8 && totalGB <= 16)
        totalGB = 16;
      else if (totalGB > 16 && totalGB <= 32)
        totalGB = 32;
      else if (totalGB > 32 && totalGB <= 64)
        totalGB = 64;
      else if (totalGB > 64 && totalGB <= 128)
        totalGB = 128;
      else if (totalGB > 128 && totalGB <= 256)
        totalGB = 256;
      else if (totalGB > 256 && totalGB <= 512)
        totalGB = 512;
      else if (totalGB > 512 && totalGB <= 1024) totalGB = 1024;

      return Future.value(
          {'free': formatMB(used), 'total': '${totalGB.round()} GB'});
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value({'free': formatMB(0.0), 'total': formatMB(0.0)});
    }
  }

  static Future<Map> getDeviceDataAndroid() async {
    try {
      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      AndroidDeviceInfo info = await deviceInfoPlugin.androidInfo;
      return Future.value({
        'model': info.model,
        'hardware': info.hardware,
        'release': info.version.release.toString()
      });
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value({
        'model': 'Not Verified',
        'hardware': 'Not Verified',
        'release': 'Not Verified'
      });
    }
  }

  static Future getDeviceDataIOS() async {
    try {
      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      IosDeviceInfo info = await deviceInfoPlugin.iosInfo;
      return Future.value(
          {'machine': info.utsname.machine, 'version': info.systemVersion});
    } on PlatformException catch (error) {
      print('error $error');
      return Future.value({'machine': 'iphone', 'version': '1.1'});
    }
  }

  static Future onConfirmYesNo(context, title, message, tapYes) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: TextCustom(message),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 1),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).pop(false);
              tapYes();
            },
            child: Text("YES"),
          ),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  static Future onConfirmImei(context, imei, tapYes, tapNo) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Confirm IMEI'),
        content: TextCustom('Do you allow to use IMEI: $imei'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () {
              Navigator.of(context).pop(false);
              tapNo();
            },
            child: Text("Not Allow"),
          ),
          SizedBox(height: 1),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).pop(false);
              tapYes();
            },
            child: Text("Allow"),
          ),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  static Future onConfirmShareLocation(context, title, message, tapYes, tapNo) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: TextCustom(message),
        actions: <Widget>[
          new GestureDetector(
            onTap: () {
              Navigator.of(context).pop(false);
              tapNo();
            },
            child: Text("Not Allow"),
          ),
          SizedBox(height: 1),
          new GestureDetector(
            onTap: () {
              Navigator.of(context).pop(false);
              tapYes();
            },
            child: Text("Allow"),
          ),
          SizedBox(height: 32),
        ],
      ),
    );
  }

  static Future onGetUserInfo() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userId = prefs.getString('userId');
    final String email = prefs.getString('email');
    final String token = prefs.getString('token');

    return Future.value({
      'userId': userId,
      'email': email,
      'token': token,
    });
  }

  static Future onGetToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');

    return Future.value(token);
  }

  static Future onInitConfirmScreen(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value(userType);
  }

  static Future onInitDiamondRating(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');
    final String transactionType = prefs.getString('transactionType');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value(
        {'userType': userType, 'transactionType': transactionType});
  }

  static Future onInitPhysicalGrading(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');
    final String transactionType = prefs.getString('transactionType');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value(
        {'userType': userType, 'transactionType': transactionType});
  }

  static Future onInitImeiInput(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    return Future.value(userType);
  }

  static Future onInitDeviceScanner(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');
    final String transactionCode = prefs.getString('transactionCode');
    final String imei = prefs.getString('imei');

    return Future.value({
      'userType': userType,
      'transactionCode': transactionCode,
      'imei': imei,
    });
  }

  static Future onInitTouchscreen(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value(userType);
  }

  static Future<void> onInitLoginScreen(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    if (userType == ConfigCustom().none ||
        userType == ConfigCustom().anonymous) {
    } else {
      if (![0, false, null, ''].contains(userType)) {
        Navigator.of(context).pushReplacementNamed(
          TransactionCodeScreen.routeName,
        );
      }
    }

    return Future.value('done');
  }

  static Future onInitBlacklist(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');
    final double long = prefs.getDouble('long');
    final double lat = prefs.getDouble('lat');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value({'userType': userType, 'long': long, 'lat': lat});
  }

  static Future onInitICloud(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');
    final String long = prefs.getString('long');
    final String lat = prefs.getString('lat');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value({'userType': userType, 'long': long, 'lat': lat});
  }

  static Future onInitWelcomeScreen(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    if (userType != ConfigCustom().free &&
        userType != ConfigCustom().fee &&
        userType != ConfigCustom().anonymous) {}

    return Future.value(userType);
  }

  static Future onInitTransactionCode(context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String userType = prefs.getString('userType');

    await prefs.remove('transactionCode');
    await prefs.remove('transactionType');
    await prefs.remove('imei');
    await prefs.remove('long');
    await prefs.remove('lat');

    if (userType != 'free' && userType != 'fee') {
      Navigator.of(context).pushReplacementNamed(
        LoginScreen.routeName,
      );
    }

    return Future.value(userType);
  }

  static Future<bool> onConfirmError(context, title, message) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(title),
            content: TextCustom(message),
            actions: <Widget>[
              new GestureDetector(
                onTap: () => Navigator.of(context).pop(true),
                child: Text("OK"),
              ),
              SizedBox(height: 32),
            ],
          ),
        ) ??
        false;
  }

  static Future<bool> onAlertConnectivity(context) {
    return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
            title: Text('Lost Internet'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom('Please connect internet and try again.'),
                SpaceCustom(),
                /*Center(
                  child: ButtonSmall('OK', 40, () {
                    Navigator.of(context).pop(false);
                  }),
                )*/
              ],
            ),
          ),
        ) ??
        false;
  }

  static Future<bool> onConfirmSomethingError(context, title, message) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(title),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom(message),
                SpaceCustom(),
                Center(
                  child: ButtonSmall('OK', 40, () {
                    Navigator.of(context).pop(false);
                  }),
                )
              ],
            ),
          ),
        ) ??
        false;
  }

  static Future<bool> onConfirmSuccess(context, title, message) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text(title),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom(message),
                SpaceCustom(),
                Center(
                  child: ButtonSmall('OK', 40, () {
                    Navigator.of(context).pop(false);
                  }),
                )
              ],
            ),
          ),
        ) ??
        false;
  }

  static Future<bool> onConfirmSuccessTouchscreen(
      context, title, message, callback) {
    return showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context) => AlertDialog(
            title: Text(title),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom(message),
                SpaceCustom(),
                Center(
                  child: ButtonSmall('OK', 40, () {
                    Navigator.of(context).pop(false);
                    callback();
                  }),
                )
              ],
            ),
          ),
        ) ??
        false;
  }

  static Future<bool> onBackPressed(context) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure ?'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom('Do you want to exit Dingtoi ?'),
                SpaceCustom(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ButtonSmall('No', 40, () {
                      Navigator.of(context).pop(false);
                    }),
                    Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: ButtonSmall('Yes', 40, () {
                        Navigator.of(context).pop(true);
                      }),
                    ),
                  ],
                )
              ],
            ),
          ),
        ) ??
        false;
  }

  static Future<bool> onBackPressedLogin(context) {
    return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Are you sure ?'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextCustom('Do you want to rescan your device on Dingtoi ?'),
                SpaceCustom(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    ButtonSmall('No', 40, () {
                      Navigator.of(context).pop(false);
                    }),
                    Container(
                      margin: const EdgeInsets.only(left: 15),
                      child: ButtonSmall('Yes', 40, () {
                        Navigator.of(context)
                            .pushReplacementNamed(LoginScreen.routeName);
                      }),
                    ),
                  ],
                )
              ],
            ),
          ),
        ) ??
        false;
  }

  static bool isEmpty(value) {
    if ([0, false, '', null, []].contains(value))
      return true;
    else
      return false;
  }

  static Map getDeviceInfoIOS(value) {
    Map result;

    switch (value) {
      case 'iPod5,1':
        result = {
          'name': 'iPod touch (5th generation)',
          'processor': 'A5',
        };
        break;
      case 'iPod7,1':
        result = {
          'name': 'iPod touch (6th generation)',
          'processor': 'A8',
        };
        break;
      case 'iPod9,1':
        result = {
          'name': 'iPod touch (7th generation)',
          'processor': 'A10',
        };
        break;
      case 'iPhone3,1':
      case 'iPhone3,2':
      case 'iPhone3,3':
        result = {
          'name': 'iPhone 4',
          'processor': 'A4',
        };
        break;
      case 'iPhone4,1':
        result = {
          'name': 'iPhone 4s',
          'processor': 'A5',
        };
        break;
      case 'iPhone5,1':
      case 'iPhone5,2':
        result = {
          'name': 'iPhone 5',
          'processor': 'A6',
        };
        break;
      case 'iPhone5,3':
      case 'iPhone5,4':
        result = {
          'name': 'iPhone 5c',
          'processor': 'A6',
        };
        break;
      case 'iPhone6,1':
      case 'iPhone6,2':
        result = {
          'name': 'iPhone 5s',
          'processor': 'A7',
        };
        break;
      case 'iPhone7,2':
        result = {
          'name': 'iPhone 6',
          'processor': 'A8',
        };
        break;
      case 'iPhone7,1':
        result = {
          'name': 'iPhone 6 Plus',
          'processor': 'A8',
        };
        break;
      case 'iPhone8,1':
        result = {
          'name': 'iPhone 6s',
          'processor': 'A9',
        };
        break;
      case 'iPhone8,2':
        result = {
          'name': 'iPhone 6s Plus',
          'processor': 'A9',
        };
        break;
      case 'iPhone8,4':
        result = {
          'name': 'iPhone SE',
          'processor': 'A9',
        };
        break;
      case 'iPhone9,1':
      case 'iPhone9,3':
        result = {
          'name': 'iPhone 7',
          'processor': 'A10 Fusion',
        };
        break;
      case 'iPhone9,2':
      case 'iPhone9,4':
        result = {
          'name': 'iPhone 7 Plus',
          'processor': 'A10 Fusion',
        };
        break;
      case 'iPhone10,1':
      case 'iPhone10,4':
        result = {
          'name': 'iPhone 8',
          'processor': 'A11 Bionic',
        };
        break;
      case 'iPhone10,2':
      case 'iPhone10,5':
        result = {
          'name': 'iPhone 8 Plus',
          'processor': 'A11 Bionic',
        };
        break;
      case 'iPhone10,3':
      case 'iPhone10,6':
        result = {
          'name': 'iPhone X',
          'processor': 'A11 Bionic',
        };
        break;
      case 'iPhone11,2':
        result = {
          'name': 'iPhone XS',
          'processor': 'A12 Bionic',
        };
        break;
      case 'iPhone11,4':
      case 'iPhone11,6':
        result = {
          'name': 'iPhone XS Max',
          'processor': 'A12 Bionic',
        };
        break;
      case 'iPhone11,8':
        result = {
          'name': 'iPhone XR',
          'processor': 'A12 Bionic',
        };
        break;
      case 'iPhone12,1':
        result = {
          'name': 'iPhone 11',
          'processor': 'A13 Bionic',
        };
        break;
      case 'iPhone12,3':
        result = {
          'name': 'iPhone 11 Pro',
          'processor': 'A13 Bionic',
        };
        break;
      case 'iPhone12,5':
        result = {
          'name': 'iPhone 11 Pro Max',
          'processor': 'A13 Bionic',
        };
        break;
      case 'iPhone12,8':
        result = {
          'name': 'iPhone SE (2nd generation)',
          'processor': 'A13 Bionic',
        };
        break;
      case 'i386':
      case 'x86_64':
        result = {
          'name': 'Simulator $value',
          'processor': 'ios',
        };
        break;
      default:
        result = {
          'name': value,
          'processor': 'ios',
        };
        break;
    }
    return result;
  }
}
